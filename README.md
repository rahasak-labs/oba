# oba

1. Kubernetes Role Base Access Control with Service Account. Read more from
   [here](https://medium.com/@lambdaEranga/kubernetes-role-base-access-control-with-service-account-e4c65e3f25cc)

2. Kubernetes HTTP API with Authentication and Authorization. Read more from
   [here](https://medium.com/@lambdaEranga/kubernetes-http-api-with-authentication-and-authorization-c0cb7051114f)
